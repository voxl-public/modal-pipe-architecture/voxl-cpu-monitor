#!/bin/bash
################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

CPU_CONTROL_PIPE=/run/mpa/cpu_monitor/control
COMMAND_SET_LED_MODE_OFF="set_led_mode_off"
COMMAND_SET_LED_MODE_ON="set_led_mode_on"
COMMAND_SET_LED_MODE_COLOR="set_led_mode_color"

SYSTEM_LED_PATH_RED="/sys/class/leds/red/brightness"
SYSTEM_LED_PATH_GREEN="/sys/class/leds/green/brightness"
SYSTEM_LED_PATH_BLUE="/sys/class/leds/blue/brightness"

VOXL_LED_BRIGHTNESS_RED=0
VOXL_LED_BRIGHTNESS_GREEN=0
VOXL_LED_BRIGHTNESS_BLUE=0

usage () {
	echo "General Usage:"
	echo ""
	echo "voxl-led on (same as white)"
	echo "voxl-led off"
	echo "voxl-led color red"
	echo "voxl-led custom 0 128 128"
	echo ""
	echo "color and custom can be appended with 'flash' to"
	echo " direct cpu monitor to flash the color"
	echo ""
	echo "Available Color Presets:"
	echo "  white, red, orange, yellow,"
	echo "  green, teal, blue, purple"
	echo ""
	exit 0
}

set_color_manually() {

	echo "CPU Monitor not running, setting colors manually"
	echo ${VOXL_LED_BRIGHTNESS_RED} > $SYSTEM_LED_PATH_RED
	echo ${VOXL_LED_BRIGHTNESS_GREEN} > $SYSTEM_LED_PATH_GREEN
	echo ${VOXL_LED_BRIGHTNESS_BLUE} > $SYSTEM_LED_PATH_BLUE

}

get_brightness_from_color () {

	case "$1" in 
		"white"|"WHITE")
			VOXL_LED_BRIGHTNESS_RED=255
			VOXL_LED_BRIGHTNESS_GREEN=255
			VOXL_LED_BRIGHTNESS_BLUE=255
			return
			;;
		"red"|"RED")
			VOXL_LED_BRIGHTNESS_RED=255
			VOXL_LED_BRIGHTNESS_GREEN=0
			VOXL_LED_BRIGHTNESS_BLUE=0
			return
			;;
		"orange"|"ORANGE")
			VOXL_LED_BRIGHTNESS_RED=255
			VOXL_LED_BRIGHTNESS_GREEN=165
			VOXL_LED_BRIGHTNESS_BLUE=0
			return
			;;
		"yellow"|"YELLOW")
			VOXL_LED_BRIGHTNESS_RED=255
			VOXL_LED_BRIGHTNESS_GREEN=255
			VOXL_LED_BRIGHTNESS_BLUE=0
			return
			;;
		"green"|"GREEN")
			VOXL_LED_BRIGHTNESS_RED=0
			VOXL_LED_BRIGHTNESS_GREEN=255
			VOXL_LED_BRIGHTNESS_BLUE=0
			return
			;;
		"teal"|"TEAL")
			VOXL_LED_BRIGHTNESS_RED=0
			VOXL_LED_BRIGHTNESS_GREEN=255
			VOXL_LED_BRIGHTNESS_BLUE=255
			return
			;;
		"blue"|"BLUE")
			VOXL_LED_BRIGHTNESS_RED=0
			VOXL_LED_BRIGHTNESS_GREEN=0
			VOXL_LED_BRIGHTNESS_BLUE=255
			return
			;;
		"purple"|"purple")
			VOXL_LED_BRIGHTNESS_RED=255
			VOXL_LED_BRIGHTNESS_GREEN=0
			VOXL_LED_BRIGHTNESS_BLUE=255
			return
			;;
		*)
			echo "Unknown color:" $1
			exit 1
			;;
	esac

}

if [ "$1" == "-h" ]; then
	usage
	exit 0
elif [ "$1" == "--help" ]; then
	usage
	exit 0
elif [ "$1" == "help" ]; then
	usage
	exit 0
fi


USER=$(whoami)
if [ "${USER}" != "root" ]; then
	echo "Run this script as the root user"
	exit 1
fi

if [ -p ${CPU_CONTROL_PIPE} ]; then
	case "$1" in
		"on"|"ON")
			echo ${COMMAND_SET_LED_MODE_ON}  >> ${CPU_CONTROL_PIPE}
			;;
		"off"|"OFF")
			echo ${COMMAND_SET_LED_MODE_OFF}  >> ${CPU_CONTROL_PIPE}
			;;
		"color"|"COLOR")
			get_brightness_from_color $2
			if [ "$#" -eq 3 ] && [ $3 = "flash" ]; then
				echo ${COMMAND_SET_LED_MODE_COLOR} ${VOXL_LED_BRIGHTNESS_RED} ${VOXL_LED_BRIGHTNESS_GREEN} ${VOXL_LED_BRIGHTNESS_BLUE} flash >> ${CPU_CONTROL_PIPE}
			elif [ "$#" -eq 2 ]; then
				echo ${COMMAND_SET_LED_MODE_COLOR} ${VOXL_LED_BRIGHTNESS_RED} ${VOXL_LED_BRIGHTNESS_GREEN} ${VOXL_LED_BRIGHTNESS_BLUE} >> ${CPU_CONTROL_PIPE}
			fi
			;;
		"custom"|"CUSTOM")
			if [ "$#" -eq 5 ] && [ $5 = "flash" ]; then
				echo ${COMMAND_SET_LED_MODE_COLOR} ${2} ${3} ${4} flash >> ${CPU_CONTROL_PIPE}
			elif [ "$#" -eq 4 ]; then
				echo ${COMMAND_SET_LED_MODE_COLOR} ${2} ${3} ${4} >> ${CPU_CONTROL_PIPE}
			fi
			;;
		*)
			usage
			exit 1
			;;
	esac
else
	case "$1" in
		"on"|"ON")
			VOXL_LED_BRIGHTNESS_RED=255
			VOXL_LED_BRIGHTNESS_GREEN=255
			VOXL_LED_BRIGHTNESS_BLUE=255
			set_color_manually
			;;
		"off"|"OFF")
			VOXL_LED_BRIGHTNESS_RED=0
			VOXL_LED_BRIGHTNESS_GREEN=0
			VOXL_LED_BRIGHTNESS_BLUE=0
			set_color_manually
			;;
		"color"|"COLOR")
			get_brightness_from_color $2
			set_color_manually
			if [ "$#" -eq 3 ] && [ $3 = "flash" ]; then
				echo "Flash not available unless voxl-cpu-monitor is running"
			fi
			;;
		"custom"|"CUSTOM")
			VOXL_LED_BRIGHTNESS_RED=$2
			VOXL_LED_BRIGHTNESS_GREEN=$3
			VOXL_LED_BRIGHTNESS_BLUE=$4
			set_color_manually
			if [ "$#" -eq 5 ] && [ $5 = "flash" ]; then
				echo "Flash not available unless voxl-cpu-monitor is running"
			fi
			;;
		*)
			usage
			exit 1
			;;
	esac
fi

exit 0
