cmake_minimum_required(VERSION 3.3)


set(SERVERNAME voxl-cpu-monitor)
add_executable(${SERVERNAME} voxl-cpu-monitor.c)
include_directories(
	../include
)

find_library(MODAL_JSON modal_json HINTS /usr/lib /usr/lib64)
find_library(MODAL_PIPE modal_pipe HINTS /usr/lib /usr/lib64)

target_link_libraries(${SERVERNAME}
	${MODAL_JSON}
	${MODAL_PIPE}
)


install(
	TARGETS ${SERVERNAME}
	LIBRARY			DESTINATION /usr/lib
	RUNTIME			DESTINATION /usr/bin
	PUBLIC_HEADER	DESTINATION /usr/include
)